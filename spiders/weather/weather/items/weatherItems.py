# -*- coding: utf-8 -*-

# Define here the models for your scraped items
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/items.html

import scrapy


class WeatherItem(scrapy.Item):
    # define the fields for your item here like:
    # name = scrapy.Field()
    # pass
    id = scrapy.Field()
    real_weather = scrapy.Field()
    real_tem = scrapy.Field()
    real_rain = scrapy.Field()
    real_wdd = scrapy.Field()
    real_wdp = scrapy.Field()
    real_aqi = scrapy.Field()
    real_aq = scrapy.Field()
    real_humidity = scrapy.Field()
    real_feelst = scrapy.Field()
    real_icomfort = scrapy.Field()
    publish_time = scrapy.Field()
    td_day_weather = scrapy.Field()
    td_day_tem = scrapy.Field()
    td_day_wdd = scrapy.Field()
    td_day_wdp = scrapy.Field()
    td_night_weather = scrapy.Field()
    td_night_tem = scrapy.Field()
    td_night_wdd = scrapy.Field()
    td_night_wdp = scrapy.Field()
    date = scrapy.Field()
    d7_info = scrapy.Field()
    h3_info = scrapy.Field()

