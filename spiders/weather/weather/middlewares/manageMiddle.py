#coding:utf8

from scrapy.exceptions import IgnoreRequest

class ManageMiddle:


    def __init__(self,crawler):
        self.crawler = crawler

    @classmethod
    def from_crawler(cls,crawler):
        obj = cls(crawler)
        return obj

    def process_request(self,request,spider):
        spider_status = request.meta.get("spider_status")
        if spider_status == "start":
            raise IgnoreRequest
        elif spider_status == "close":
            self.crawler.engine.close_spider(spider,"closing_from_manage")
            raise IgnoreRequest



