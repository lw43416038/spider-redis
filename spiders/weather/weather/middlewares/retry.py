#coding:utf8
import logging
from scrapy.downloadermiddlewares.retry import RetryMiddleware
from scrapy.utils.python import global_object_name

logger = logging.getLogger(__name__)

class DisRetryMiddleware(RetryMiddleware):

    def __init__(self,settings,crawler):
        super().__init__(settings)
        self.crawler = crawler

    @classmethod
    def from_crawler(cls, crawler):
        settings = crawler.settings
        return cls(settings,crawler)

    def _retry(self, request, reason, spider):
        retries = request.meta.get('retry_times', 0) + 1

        retry_times = self.max_retry_times

        if 'max_retry_times' in request.meta:
            retry_times = request.meta['max_retry_times']

        stats = spider.crawler.stats
        if retries <= retry_times:
            # logger.debug("Retrying %(request)s (failed %(retries)d times): %(reason)s",
            #              {'request': request, 'retries': retries, 'reason': reason},
            #              extra={'spider': spider})
            retryreq = request.copy()
            retryreq.meta['retry_times'] = retries
            retryreq.dont_filter = True
            retryreq.priority = request.priority + self.priority_adjust

            if isinstance(reason, Exception):
                reason = global_object_name(reason.__class__)

            stats.inc_value('retry/count')
            stats.inc_value('retry/reason_count/%s' % reason)
            #放入到redis queue
            queue = spider.queue
            queue.push(retryreq)
            logger.debug("push %(request)s (failed %(retries)d times): %(reason)s again",
                         {'request': request, 'retries': retries, 'reason': reason},
                         extra={'spider': spider})

        else:
            stats.inc_value('retry/max_reached')
            logger.debug("Gave up retrying %(request)s (failed %(retries)d times): %(reason)s",
                         {'request': request, 'retries': retries, 'reason': reason},
                         extra={'spider': spider})