# -*- coding: utf-8 -*-
import json
import re
from scrapy import Spider
from utils.request import RequestNoFilter
from ..items.weatherItems import WeatherItem
from collections import OrderedDict
from datetime import datetime,timedelta
from scrapy.selector import Selector
from db.sessions import Session_Factory
from db.models import WeatherArea as WA
from db.models import Weather as Wea
from sqlalchemy import func

datetime_format = "%Y-%m-%d %H:%M:%S"
weather_info = {"0":"晴","1":"多云","2":"阴","3":"阵雨","4":"雷阵雨","5":"雷阵雨","6":"雨夹雪","7":"小雨","8":"中雨","9":"大雨","10":"暴雨","11":"大暴雨","12":"特大暴雨","13":"阵雪","14":"小雪","15":"中雪","16":"大雪","17":"暴雪","18":"雾","19":"暴雨","20":"台风","21":"中雨","22":"大雨","23":"暴雨","24":"大暴雨","25":"特大暴雨","26":"中雪","27":"大雪","28":"暴雪","29":"浮尘","30":"","31":"","32":"中雨","33":"中雪","53":"霾"}
icomfort_info = {'i9999':'','i4':'很热，极不适应', 'i3':'热，很不舒适', 'i2':'暖，不舒适', 'i1':'温暖，较舒适', 'i0':'舒适，最可接受', 'i-1':'凉爽，较舒适', 'i-2':'凉，不舒适', 'i-3':'冷，很不舒适', 'i-4':'很冷，极不适应'}
week_info = {"星期一":1,"星期二":2,"星期三":3,"星期四":4,"星期五":5,"星期六":6,"星期日":7,}
date_chi_info = ["今天","明天","后天"]


def get_publish_time(result,id):
    for publish_time,id_r in result:
        if id_r == id:
            if publish_time:
                return publish_time.strftime(datetime_format)

def wdp_from_wds(wds):
    if not isinstance(wds,(int,float)):
        raise TypeError("must be int or float")
    if wds < 0.2:
        wdp = 0
    elif wds < 1.6:
        wdp = 1
    elif wds < 3.4:
        wdp = 2
    elif wds < 5.5:
        wdp = 3
    elif wds < 8:
        wdp = 4
    elif wds < 10.8:
        wdp = 5
    elif wds < 13.9:
        wdp = 6
    elif wds < 17.2:
        wdp = 7
    elif wds < 20.8:
        wdp = 8
    elif wds < 24.5:
        wdp = 9
    elif wds < 28.5:
        wdp = 10
    elif wds < 32.6:
        wdp = 11
    elif wds < 37:
        wdp = 12
    elif wds < 41.4:
        wdp = 13
    elif wds < 46.1:
        wdp = 14
    elif wds < 50.9:
        wdp = 15
    elif wds < 56:
        wdp = 16
    else:
        wdp = 17
    return wdp


class WeatherSpider(Spider):
    name = 'weather'

    def start_requests(self):
        session = Session_Factory()
        fields = [WA.id,WA.code_nmc,WA.prov_code,WA.sp_nmc,WA.areaId]
        areas = session.query(*fields).filter(WA.nmc_status == "1",WA.areaId.isnot(None)).all()
        publish_time_info = session.query(func.max(Wea.publish_time),Wea.id).group_by(Wea.id).all()
        session.close()
        for item in areas:
            wea_id, code_nmc, prov_code, sp_nmc, areaId = item
            publish_time = get_publish_time(publish_time_info,wea_id)
            meta = dict(wea_id= wea_id,code_nmc=code_nmc,prov_code=prov_code,sp_nmc=sp_nmc,areaId=areaId,publish_time = publish_time)
            url_real = "http://www.nmc.cn/f/rest/real/%s"%code_nmc
            yield RequestNoFilter(url_real,callback=self.parse_real,meta=meta)

    def parse_real(self, response):

        meta = response.meta
        publish_time = None
        temperature = None  # 气温 摄氏度℃
        humidity = None  # 湿度%
        rain = None  # 降水量
        wdd = None  # 风向
        wdp = None  # 风力
        icomfort = None  # 舒适度
        feelst = None  # 体感温度 ℃
        alert = None
        text =response.text
        publish_time_o = meta["publish_time"]
        try:
            rj = json.loads(text)
        except:
            pass
        else:
            publish_time = rj["publish_time"]
            if publish_time == publish_time_o:
                return
            weather = rj["weather"]
            wind = rj["wind"]
            temperature = weather["temperature"]  # 气温 摄氏度℃
            humidity = weather["humidity"]  # 湿度%
            rain = weather["rain"]  # 降水量
            if rain == 9999:
                rain = None
            wdd = wind["direct"]  # 风向
            wdp = wind["power"]  # 风力
            icomfort = icomfort_info["i" + str(weather["icomfort"])]  # 舒适度
            feelst = weather["feelst"]  # 体感温度 ℃
            warn = rj['warn']
            alert = "" if warn["alert"] == "9999" else warn["alert"]
        result = dict()
        result["publish_time"] = publish_time
        result["temperature"] = temperature
        result["humidity"] = humidity
        result["rain"] = rain
        result["wdd"] = wdd
        result["wdp"] = wdp
        result["icomfort"] = icomfort
        result["feelst"] = feelst
        result["alert"] = alert
        meta["result"] = result
        areaId = meta["areaId"]
        url = "http://d1.weather.com.cn/sk_2d/%s.html?_=1521708275572" % areaId
        headers = dict()
        headers[
            "User-Agent"] = "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/64.0.3282.186 Safari/537.36"
        headers["Referer"] = "http://www.weather.com.cn/weather1d/101010100.shtml"
        yield RequestNoFilter(url,callback=self.parse_aq,meta = meta,encoding="utf8",headers =headers,priority=1)

    def parse_aq(self, response):

        text = response.text
        dataSK = eval(re.findall("dataSK = (\{.*?\})", text)[0])
        weather = dataSK["weather"]
        aqi = dataSK["aqi"]
        if aqi == "":
            aqi = None
            aq = None
        else:
            try:
                temp = int(aqi)
                if temp < 51:
                    aq = "优"
                elif temp < 101:
                    aq = "良"
                elif temp < 151:
                    aq = "轻度污染"
                elif temp < 201:
                    aq = "中度污染"
                elif temp < 301:
                    aq = "重度污染"
                else:
                    aq = "严重污染"
            except:
                aq = ""
                aqi = None

        meta = response.meta
        meta["result"]["aq"] = aq
        meta["result"]["aqi"] = aqi
        meta["result"]["weather"] = weather
        prov_code = meta["prov_code"]
        sp = meta["sp_nmc"]
        url = "http://www.nmc.cn/publish/forecast/%s/%s.html" % (prov_code, sp)
        yield RequestNoFilter(url,callback=self.parse_d7_h3,meta = meta,encoding="utf8",priority=2)


    def parse_d7_h3(self, response):
        xm = Selector(response)
        items = xm.xpath('//div[@class="today"]/table/tbody')
        d7_data = OrderedDict()
        n = 1
        now = datetime.today()
        for item in items:
            temp = dict()
            if n < 4:
                week_chi = item.xpath("./tr[1]/td[1]/p[2]/text()").extract()[0].strip()
                date_str = item.xpath("./tr[1]/td[2]/text()").extract()[0].strip()
            else:
                week_chi = item.xpath("./tr[1]/td[2]/text()").extract()[0].strip()
                date_str = item.xpath("./tr[1]/td[1]/text()").extract()[0].strip()
            date = datetime.strptime(str(now.year) + date_str, "%Y%m月%d日").strftime("%Y-%m-%d")
            if n > len(date_chi_info):
                date_chi = ""
            else:
                date_chi = date_chi_info[n - 1]
            temp["date_chi"] = date_chi
            temp["week_chi"] = week_chi
            temp["date"] = date
            if len(item.xpath('./tr[3]/td')) == 1:
                temp["weather_day"] = None
                temp["weather_night"] = item.xpath("./tr[3]/td[1]/text()").extract()[0].strip()
                temp["tem_day"] = None
                temp["tem_night"] = item.xpath("./tr[4]/td[1]/text()").extract()[0].strip().replace("℃", "")
                temp["wdd_day"] = None
                temp["wdd_night"] = item.xpath("./tr[5]/td[1]/text()").extract()[0].strip()
                temp["wdp_day"] = None
                temp["wdp_night"] = item.xpath("./tr[6]/td[1]/text()").extract()[0].strip()
            else:
                temp["weather_day"] = item.xpath("./tr[3]/td[1]/text()").extract()[0].strip()
                temp["weather_night"] = item.xpath("./tr[3]/td[2]/text()").extract()[0].strip()
                temp["tem_day"] = item.xpath("./tr[4]/td[1]/text()").extract()[0].strip().replace("℃", "")
                temp["tem_night"] = item.xpath("./tr[4]/td[2]/text()").extract()[0].strip().replace("℃", "")
                temp["wdd_day"] = item.xpath("./tr[5]/td[1]/text()").extract()[0].strip()
                temp["wdd_night"] = item.xpath("./tr[5]/td[2]/text()").extract()[0].strip()
                temp["wdp_day"] = item.xpath("./tr[6]/td[1]/text()").extract()[0].strip()
                temp["wdp_night"] = item.xpath("./tr[6]/td[2]/text()").extract()[0].strip()
            d7_data["d" + str(n)] = temp
            n += 1
        h3_data = list()
        xm_h3 = xm.xpath("//div[@class='hour3']")[0]
        cn = len(xm_h3.xpath('./div[1]/div'))
        date = now.strftime("%Y-%m-%d ")
        date_t = (now + timedelta(days=1)).strftime("%Y-%m-%d ")
        for i in range(2, cn + 1):
            temp = dict()
            time_now = now.strftime("%H:%M")
            time = xm_h3.xpath("./div[1]/div[%d]/text()" % i).extract()[0].strip()
            if "日" in time:
                time = time.split("日")[-1]
            if time > time_now:
                date_item = date
            else:
                date_item = date_t
            weather_img_url = xm_h3.xpath("./div[2]/div[%d]/img/@src" % i).extract()[0].strip()
            tem = xm_h3.xpath("./div[3]/div[%d]/text()" % i).extract()[0].strip().replace("℃", "")
            weather_id = re.findall("/(\d+)\.png", weather_img_url)[0]
            if weather_id in weather_info:
                weather = weather_info[weather_id]
            else:
                # logger.info("未知天气情况<%s>,<%s>" % (weather_id, sp))
                weather = ""
            rain_temp = xm_h3.xpath("./div[4]/div[%d]/text()" % i).extract()[0].strip()
            if rain_temp == "无降水":
                rain = 0
            else:
                rain = float(rain_temp.replace("毫米", ""))
            wds_temp = xm_h3.xpath("./div[5]/div[%d]/text()" % i).extract()[0].strip()
            wds = float(wds_temp.replace("米/秒", ""))
            wdp = str(wdp_from_wds(wds)) + "级"
            wdd = xm_h3.xpath("./div[6]/div[%d]/text()" % i).extract()[0].strip()
            humidity = xm_h3.xpath("./div[8]/div[%d]/text()" % i).extract()[0].strip().replace("%", "")
            cloud = xm_h3.xpath("./div[9]/div[%d]/text()" % i).extract()[0].strip().replace("%", "")
            vis = xm_h3.xpath("./div[10]/div[%d]/text()" % i).extract()[0].strip()
            temp["time"] = time
            temp["date"] = date_item
            temp["tem"] = tem
            temp["weather"] = weather
            temp["wds"] = wds
            temp["wdp"] = wdp
            temp["wdd"] = wdd
            temp["humidity"] = humidity
            temp["cloud"] = cloud
            temp["vis"] = vis
            temp["rain"] = rain
            h3_data.append(temp)
        result = response.meta["result"]
        td_data = d7_data["d1"]
        values = WeatherItem()
        wea_id = response.meta["wea_id"]
        values["id"] = wea_id
        values["real_weather"] = result["weather"]
        values["real_tem"] = result["temperature"]
        values["real_rain"] = result["rain"]
        values["real_wdd"] = result["wdd"]
        values["real_wdp"] = result["wdp"]
        values["real_aqi"] = result["aqi"]
        values["real_aq"] = result["aq"]
        values["real_humidity"] = result["humidity"]
        values["real_feelst"] = result["feelst"]
        values["real_icomfort"] = result["icomfort"]
        values["publish_time"] = result["publish_time"]
        values["td_day_weather"] = td_data["weather_day"]
        values["td_day_tem"] = td_data["tem_day"]
        values["td_day_wdd"] = td_data["wdd_day"]
        values["td_day_wdp"] = td_data["wdp_day"]
        values["td_night_weather"] = td_data["weather_night"]
        values["td_night_tem"] = td_data["tem_night"]
        values["td_night_wdd"] = td_data["wdd_night"]
        values["td_night_wdp"] = td_data["wdp_night"]
        values["date"] = td_data["date"]
        values["d7_info"] = json.dumps(d7_data,ensure_ascii=False)
        values["h3_info"] = json.dumps(h3_data,ensure_ascii=False)
        return values

