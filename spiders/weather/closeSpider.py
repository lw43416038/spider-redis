
import pickle
from scrapy import Request
from db.common_redis import RedisUtils
from scrapy.utils.reqser import request_to_dict
from scrapy_redis.defaults import SCHEDULER_QUEUE_KEY


spider_name = "weather"
key = SCHEDULER_QUEUE_KEY%dict(spider =spider_name)
request = Request(":",meta=dict(spider_status = "close"))
data = pickle.dumps(request_to_dict(request))
redisUtil = RedisUtils()
redisUtil.zadd(key,data,-999)
