

from scrapy import Request


class RequestNoFilter(Request):

    def __init__(self,*args,**kw):
        kw["dont_filter"] = True
        super().__init__(*args,**kw)