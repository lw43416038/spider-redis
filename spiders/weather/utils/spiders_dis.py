#coding:utf8
import os
import logging
from logging.handlers import RotatingFileHandler
from scrapy import Request
from scrapy_redis.spiders import RedisSpider
from scrapy_redis import defaults
from scrapy.utils.misc import load_object
from conf.flag import IS_MASTER

from scrapy.spiders import Spider

root_dir = os.getcwd()
log_dir = os.path.join(root_dir,"logs")
os.path.exists(log_dir) or os.mkdir(log_dir)

class BaseSpider(RedisSpider):

    @classmethod
    def update_settings(cls, settings):
        # some_defaults = {
        #     "LOG_LEVEL":"DEBUG",
        #     "LOG_FORMAT":"%(asctime)s %(levelname)s %(filename)s line:%(lineno)d %(thread)d %(message)s",
        #     "LOG_FILE":os.path.join(log_dir,cls.name + ".log"),
        # }
        # settings.setdict(some_defaults, priority='spider')
        settings.setdict(cls.custom_settings or {}, priority='spider')

    @property
    def logger(self):
        logger = logging.getLogger(self.name)
        logger.addHandler(logging.StreamHandler())
        return logging.LoggerAdapter(logger, {'spider': self})



if IS_MASTER:
    class DisSpider(RedisSpider):

        def start_requests(self):
            yield Request(":",meta=dict(spider_status = "start"))

        @classmethod
        def update_settings(cls, settings):
            settings.setdict({"SCHEDULER_FLUSH_ON_START_DIS":True}, priority='spider')
            settings.setdict(cls.custom_settings or {}, priority='spider')


        def setup_redis(self, crawler=None):
            SCHEDULER_FLUSH_ON_START_DIS = self.settings.get("SCHEDULER_FLUSH_ON_START_DIS")
            super().setup_redis()
            x = list(self.start_requests_dis())
            queue_cls = self.settings.get(
                'SCHEDULER_QUEUE_CLASS', defaults.SCHEDULER_QUEUE_CLASS,
            )
            queue_key = self.settings.get(
                'SCHEDULER_QUEUE_KEY', defaults.SCHEDULER_QUEUE_KEY,
            )
            serializer = self.settings.get("SCHEDULER_SERIALIZER")
            try:
                self.queue = load_object(queue_cls)(
                    server=self.server,
                    spider=self,
                    key=queue_key % {'spider': self.name},
                    serializer=serializer,
                )
            except TypeError as e:
                raise ValueError("Failed to instantiate queue class '%s': %s",
                                 queue_cls, e)
            if SCHEDULER_FLUSH_ON_START_DIS:
                self.queue.clear()
            for i in x:
                self.queue.push(i)

        def start_requests_dis(self):
            raise NotImplementedError

        def spider_idle(self):
            pass

else:
    class DisSpider(RedisSpider):

        def start_requests(self):
            yield Request(":",meta=dict(spider_status = "start"))


        def setup_redis(self, crawler=None):
            super().setup_redis()
            queue_cls = self.settings.get(
                'SCHEDULER_QUEUE_CLASS', defaults.SCHEDULER_QUEUE_CLASS,
            )
            queue_key = self.settings.get(
                'SCHEDULER_QUEUE_KEY', defaults.SCHEDULER_QUEUE_KEY,
            )
            serializer = self.settings.get("SCHEDULER_SERIALIZER")
            try:
                self.queue = load_object(queue_cls)(
                    server=self.server,
                    spider=self,
                    key=queue_key % {'spider': self.name},
                    serializer=serializer,
                )
            except TypeError as e:
                raise ValueError("Failed to instantiate queue class '%s': %s",
                                 queue_cls, e)