# coding:utf8
from sqlalchemy import Column, String, Integer, DateTime, Text, TIMESTAMP, FLOAT
from sqlalchemy.ext.declarative import declarative_base
from sqlalchemy.dialects.mysql import LONGTEXT, CHAR

class DataDB():
    pass
class SpiderDB():
    pass
class LogDB():
    pass
class ComDB():
    pass
class SJZHDB():
    pass

Base = declarative_base()

class News(Base):
    """
    新闻
    """
    __tablename__ = 'news'
    id = Column(Integer(), primary_key=True)
    link = Column(String())
    title = Column(String())
    newsId = Column(String(64))
    text = Column(LONGTEXT())
    source = Column(String())
    img_url = Column(String())
    thumbnail_url = Column(String())
    categories = Column(String())
    typeid = Column(String())
    original_type = Column(String())
    editor = Column(String())
    publish_time = Column(DateTime())


class Joke(Base):
    __tablename__ = "joke"
    id = Column(Integer(), primary_key=True)
    title = Column(String(90))
    text = Column(Text())
    category = Column(String(20))
    publishTime = Column(DateTime())
    tms = Column(TIMESTAMP(), default="CURRENT_TIMESTAMP")
    md5 = Column(String(64))


class EnWord(Base):
    __tablename__ = 'enWord'
    id = Column(Integer(), primary_key=True)
    title = Column(String(130))
    word = Column(String(40))
    des = Column(String(255))
    wordy = Column(String(32))
    sound = Column(String(255))
    class_id = Column(Integer())
    word_num = Column(Integer())
    title_class = Column(String(50))
    course_num = Column(Integer())
    course = Column(Integer())


class ScenicSpot(Base):
    __tablename__ = 'scenicSpot'

    id = Column(Integer(), primary_key=True)
    name = Column(String(130))
    proId = Column(Integer())
    proName = Column(String(130))
    cityId = Column(Integer())
    cityName = Column(String(130))
    areaId = Column(Integer())
    areaName = Column(Text())
    location = Column(Text())
    address = Column(Text())
    content = Column(Text())
    coupon = Column(Text())
    attention = Column(Text())
    opentime = Column(Text())
    picList = Column(Text())
    summary = Column(Text())
    priceList = Column(Text())


class Riddle(Base):
    __tablename__ = 'riddle'

    id = Column(Integer(), primary_key=True)
    title = Column(String(255))
    answer = Column(String(255))
    typeName = Column(String(255))
    typeId = Column(String(255))
    page = Column(Integer())


class Weather(Base):
    __tablename__ = "spider_weather_data"

    item_id = Column(Integer(), primary_key=True)
    id = Column(Integer())
    real_weather = Column(String(32))
    real_tem = Column(FLOAT(10))
    real_rain = Column(FLOAT(10))
    real_wdd = Column(String(32))
    real_wdp = Column(String(32))
    real_aqi = Column(Integer())
    real_aq = Column(String(32))
    real_humidity = Column(FLOAT(10))
    real_feelst = Column(FLOAT(10))
    real_icomfort = Column(String(32))
    publish_time = Column(DateTime())
    td_day_weather = Column(String(32))
    td_day_tem = Column(FLOAT(10))
    td_day_wdd = Column(String(32))
    td_day_wdp = Column(String(32))
    td_night_weather = Column(String(32))
    td_night_tem = Column(FLOAT(10))
    td_night_wdd = Column(String(32))
    td_night_wdp = Column(String(32))
    date = Column(DateTime())
    d7_info = Column(Text())
    h3_info = Column(Text())


class WeatherArea(Base,SpiderDB):
    __tablename__ = "chain_area_weather_info"

    id = Column(Integer(), primary_key=True)
    province = Column(String(20))
    city = Column(String(20))
    county = Column(String(20))
    postCode = Column(Integer())
    areaId = Column(Integer())
    code_nmc = Column(CHAR(5))
    sp_nmc = Column(String(20))
    sp_f = Column(CHAR(1))
    prov_code = Column(CHAR(3))
    tms = Column(TIMESTAMP(), default="CURRENT_TIMESTAMP")
    nmc_status = Column(CHAR(2))
    weather_id = Column(Integer())


class CompanyTax(Base,SJZHDB):
    __tablename__ = "company_tax"
    id = Column(Integer(), primary_key=True)
    com_id = Column(Integer())
    company = Column(String(255))
    com_addr = Column(String(255))
    phone = Column(String(50))
    bank = Column(String(255))
    bankcard = Column(String(64))
    taxNo = Column(String(128))


class CompanyTaxNot(Base,SJZHDB):
    __tablename__ = "company_tax_not"

    id = Column(Integer(), primary_key=True)
    company = Column(String(255))


class CompanyId(Base,ComDB):
    __tablename__ = "company_id"
    company_id = Column(Integer(), primary_key=True)
    name = Column(String(255))
    area = Column(String(20))
    keyword = Column(String(20))
    has_gsxt = Column(Integer())
    weight = Column(Integer())
    # tms = Column(TIMESTAMP(), default="CURRENT_TIMESTAMP")


class ShiXin(Base):
    __tablename__ = "realtime_shixin_data"

    id = Column(Integer(), primary_key=True)
    name = Column(String(64))
    cardNum = Column(String(64))
    md5 = Column(CHAR(32))
    age = Column(Integer())
    sex = Column(String(10))
    areaName = Column(String(64))
    courtName = Column(String(64))
    caseCode = Column(String(64))
    duty = Column(Text(64))
    performance = Column(String(255))
    disruptTypeName = Column(String(255))
    publishDate = Column(String(32))

class App_Info(Base,SpiderDB):
    __tablename__ = "realtime_app_info"
    id = Column(Integer(), primary_key=True)
    project = Column(CHAR(16))
    app = Column(CHAR(32))
    app_branch = Column(CHAR(32))
    params = Column(CHAR(255))
    app_no = Column(CHAR(10))
    status = Column(Integer())
    is_qStatus = Column(Integer())
    have_message = Column(Integer())


class Status_info(Base,SpiderDB):
    __tablename__ = "realtime_status_info"
    id = Column(Integer(), primary_key=True)
    project = Column(CHAR(16))
    status = Column(CHAR(10))
    desc = Column(CHAR(255))



