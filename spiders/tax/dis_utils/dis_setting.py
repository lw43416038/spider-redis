import os

LOG_DIR = "/var/log/docker"
os.path.exists(LOG_DIR) or os.mkdir(LOG_DIR)
LOG_FILE = os.path.join(LOG_DIR ,"tax.log")
LOG_LEVEL = "INFO"

DOWNLOAD_TIMEOUT = 10

RETRY_TIMES = 20

DOWNLOADER_MIDDLEWARES = {
    'scrapy.downloadermiddlewares.retry.RetryMiddleware': None,
    'dis_utils.downloadMiddleware.Retry': 543,
    'dis_utils.downloadMiddleware.Proxy':555,
}


