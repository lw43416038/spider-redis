from hashlib import md5
from scrapy_redis.dupefilter import RFPDupeFilter

# key_crawling = "tax:crawling"

def get_m5(text):
    m5 = md5()
    m5.update(text.encode())
    return m5.hexdigest()[:16]


class RFPDupeFilter_name(RFPDupeFilter):

    def request_seen(self, request):
        fp = self.request_fingerprint(request)
        added = self.server.sadd(self.key, fp)
        return added == 0

    def request_fingerprint(self, request):
        name = request.meta["name"]
        return get_m5(name)
