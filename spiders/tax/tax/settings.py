# -*- coding: utf-8 -*-


BOT_NAME = 'tax'

SPIDER_MODULES = ['tax.spiders']
NEWSPIDER_MODULE = 'tax.spiders'

ITEM_PIPELINES = {
    'tax.pipelines.TaxPipeline': 300,
}

# Crawl responsibly by identifying yourself (and your website) on the user-agent
USER_AGENT = 'Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/68.0.3440.106 Safari/537.36'

# Obey robots.txt rules
ROBOTSTXT_OBEY = False

DUPEFILTER_CLASS = "tax.dupefilter_by_name.RFPDupeFilter_name"
SCHEDULER = "scrapy_redis.scheduler.Scheduler"
SCHEDULER_PERSIST = True
# REDIS_URL = "redis://:yWY0a7fKB3K1pTFo@106.14.254.167:6381/2"
REDIS_URL = "redis://:yWY0a7fKB3K1pTFo@10.28.162.198:6381/2"
REDIS_DATA_KEY = "tax:data"
REDIS_PROXY_KEY = "tax:proxy"

CONCURRENT_REQUESTS = 8
CONCURRENT_REQUESTS_PER_DOMAIN = 128

DOWNLOAD_TIMEOUT = 10


from dis_utils.dis_setting import *
