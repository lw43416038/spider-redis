# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import json


class BaiduKnowledgePipeline(object):
    def process_item(self, item, spider):
        # name = item["data"]["entName"]
        # key = spider.name + ":dupefilter" + ":" + name + ":" + "crawled"
        # server = spider.server
        # page = item.get('page', '')
        # type = item.get('type', '')
        # value = type + " " + page
        # server.sadd(key, value)
        # server.expire(key, 10 * 60)
        data = json.dumps(item, ensure_ascii=False)

        if 'icpinfoAjax' in item:
            spider.server.rpush("icpinfo:data", data)
        elif 'markAjax' in item:
            spider.server.rpush("mark:data", data)
        elif 'patentAjax' in item:
            spider.server.rpush("patent:data", data)
        elif 'copyrightAjax' in item:
            spider.server.rpush("copyright:data", data)
        return item
