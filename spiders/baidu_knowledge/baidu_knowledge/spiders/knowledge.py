# -*- coding: utf-8 -*-
import scrapy
from scrapy_redis.spiders import RedisSpider
from scrapy.exceptions import DontCloseSpider
from selenium import webdriver
import json
import time
import re
import copy
import os

pwd = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
phantomjs_path = os.path.join(pwd, "phantomjs")
driver = webdriver.PhantomJS(executable_path=phantomjs_path)
driver.get("https://www.baidu.com/")

key_keyword = "knowledge:keyword"


class KnowledgeSpider(RedisSpider):
    name = 'knowledge'
    allowed_domains = ['baidu.com']

    def parse(self, response):
        meta = response.meta
        source = response.xpath("/html/body/script[4]/text()").extract_first()
        tkatrr = re.search(r"getAttribute\('(.*?)'\)", source).group(1)
        source = 'B=' + re.sub(r'\(function\(\).*\(\);', '', source)
        tk = re.compile(r'{}="(.*?)"'.format(tkatrr)).search(response.body.decode()).group(1)
        bid = response.xpath("//span[@id='baiducode']/text()").extract_first()
        driver.execute_script(source)
        tot = driver.execute_script("return B('{}','{}')".format(tk, bid))
        pid = re.search(r'"result":{"pid":"(.*?)"', response.body.decode()).group(1)
        icpinfoPage = response.xpath(
            "//ul[@class='zx-detail-tab']/li[3]/span[2]/text()").extract_first()
        icpinfoPage = int(icpinfoPage.replace('+', ''))
        if icpinfoPage:
            icpinfo_url = "https://xin.baidu.com/detail/icpinfoAjax?pid={}&tot={}&p=1&_={}".format(pid, tot, int(
                time.time() * 1000))
            meta_icpinfo = copy.deepcopy(meta)
            meta_icpinfo['page'], meta_icpinfo['ajaxType'] = 'p1', 'icpinfo'

            mark_url = "https://xin.baidu.com/detail/markAjax?pid={}&tot={}&p=1&_={}".format(pid, tot, int(
                time.time() * 1000))
            meta_mark = copy.deepcopy(meta)
            meta_mark['page'], meta_mark['ajaxType'] = 'p1', 'mark'

            patent_url = 'https://xin.baidu.com/detail/patentAjax?pid={}&p=1&f=&tot={}&_={}'.format(pid, tot, int(
                time.time() * 1000))
            meta_patent = copy.deepcopy(meta)
            meta_patent['page'], meta_patent['ajaxType'] = 'p1', 'patent'

            copyright_url = 'https://xin.baidu.com/detail/copyrightAjax?pid={}&p=1&f=&tot={}&_=1540450216480'.format(
                pid, tot, int(
                    time.time() * 1000))
            meta_copyright = copy.deepcopy(meta)
            meta_copyright['page'], meta_copyright['ajaxType'] = 'p1', 'copyright'

            yield scrapy.Request(icpinfo_url, callback=self.parse_list, priority=50, meta=meta_icpinfo,
                                 dont_filter=True)
            yield scrapy.Request(mark_url, callback=self.parse_list, priority=50, meta=meta_mark, dont_filter=True)
            yield scrapy.Request(patent_url, callback=self.parse_list, priority=50, meta=meta_patent, dont_filter=True)
            yield scrapy.Request(copyright_url, callback=self.parse_list, priority=50, meta=meta_copyright,
                                 dont_filter=True)

    def parse_list(self, response):
        meta = response.meta
        item = json.loads(response.body.decode())
        pageCount = item['data']['pageCount']
        if pageCount != 0:
            for i in range(1, pageCount + 1):
                url = re.sub(r'p=\d+', 'p={}'.format(i), response.url)
                meta_update = copy.deepcopy(meta)
                meta_update['page'] = 'p{}'.format(i)
                yield scrapy.Request(url, callback=self.parse_detail, priority=50, meta=meta_update)

    def parse_detail(self, response):
        meta = response.meta
        item = json.loads(response.body.decode())
        item["data"]["referer"] = meta['url']
        item["data"]["entName"] = meta['comName']
        item['page'] = meta['page']
        item['type'] = meta['ajaxType']
        if 'icpinfoAjax' in response.url:
            item['icpinfoAjax'] = 'icpinfoAjax'
            self.add_fingerprint(item)
            yield item
        elif 'copyrightAjax' in response.url:
            item['copyrightAjax'] = 'copyrightAjax'
            self.add_fingerprint(item)
            yield item
        elif 'markAjax' in response.url:
            item['markAjax'] = 'markAjax'
            self.add_fingerprint(item)
            pageCount = item['data']['pageCount']
            if pageCount != 0:
                for i in item['data']['list']:
                    contentId = i['contentId']
                    referId = i['referId']
                    markRegNo = i['markRegNo']
                    meta_mark = {'markRegNo': markRegNo, 'entName': meta['comName']}
                    url = 'https://xin.baidu.com/detail/markContentAjax?contentId={}&referId={}&_={}'.format(
                        contentId, referId, int(time.time() * 1000))
                    yield scrapy.Request(url, callback=self.parse_result, priority=70, dont_filter=True, meta=meta_mark)
        elif 'patentAjax' in response.url:
            item['patentAjax'] = 'patentAjax'
            self.add_fingerprint(item)
            pageCount = item['data']['pageCount']
            if pageCount != 0:
                for i in item['data']['list']:
                    referId = i['referId']
                    meta_patent = {'entName': meta['comName']}
                    url = 'https://xin.baidu.com/detail/patentContentAjax?referId={}&_=1540371004681'.format(
                        referId, int(time.time() * 1000))
                    yield scrapy.Request(url, callback=self.parse_result, priority=70, dont_filter=True,
                                         meta=meta_patent)

    def parse_result(self, response):
        meta = response.meta
        item = json.loads(response.body.decode())
        if 'markContentAjax' in response.url:
            item['data']['markRegNo'] = meta.get('markRegNo')
            item['data']['entName'] = meta.get('entName')
            item['markAjax'] = 'markAjax'
        elif 'patentContentAjax' in response.url:
            item['data']['entName'] = meta.get('entName')
            item['patentAjax'] = 'patentAjax'
        yield item

    def add_fingerprint(self, item):
        name = item["data"]["entName"]
        key = self.name + ":dupefilter" + ":" + name + ":" + "crawled"
        server = self.server
        page = item.get('page', '')
        type = item.get('type', '')
        value = type + " " + page
        server.sadd(key, value)
        server.expire(key, 10 * 60)

    def spider_idle(self):
        server = self.server
        for i in range(2000):
            keyword = server.blpop(key_keyword)[1].decode()
            sp_list = keyword.split(', ')
            url = sp_list[0]
            comName = sp_list[1]
            meta = {"url": url, 'comName': comName}
            req = scrapy.Request(url, callback=self.parse, dont_filter=True, meta=meta, priority=20)
            self.crawler.engine.crawl(req, spider=self)
        raise DontCloseSpider
