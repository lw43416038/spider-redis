from hashlib import md5
from scrapy_redis.dupefilter import RFPDupeFilter


class RFPDupeFilter_name(RFPDupeFilter):

    def request_seen(self, request):
        name = request.meta["comName"]
        key = self.key + ":" + name + ":" + "crawled"
        page = request.meta.get('page', '')
        type = request.meta.get('ajaxType', '')
        year = request.meta.get('year', '')
        value = type + " " + page + " " + year
        # self.server.sadd(key,value)
        isExist = self.server.sismember(key, value)

        return isExist

    # def request_fingerprint(self, request):
    #     name = request.meta["comName"]
    #     page = request.meta.get('page', '')
    #     type = request.meta.get('ajaxType', '')
    #     year = request.meta.get('year', '')
    #     fp = md5()
    #     fp.update(name.encode())
    #     fp.update(page.encode())
    #     fp.update(type.encode())
    #     fp.update(year.encode())
    #
    #     return fp.hexdigest()
