# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import json


class BaiduDetailPipeline(object):
    def process_item(self, item, spider):
        name = item["data"]["entName"]
        key = spider.name + ":dupefilter" + ":" + name + ":" + "crawled"
        server = spider.server
        page = item.get('page', '')
        type = item.get('type', '')
        year = item.get('year', '')
        value = type + " " + page + " " + year
        server.sadd(key, value)
        server.expire(key, 10 * 60)

        data = json.dumps(item, ensure_ascii=False)
        if 'changeajax' in item:
            spider.server.rpush("change:data", data)
        elif 'investajax' in item:
            spider.server.rpush("invest:data", data)
        elif 'branchajax' in item:
            spider.server.rpush("branch:data", data)
        elif 'annualajax' in item:
            spider.server.rpush("annual:data", data)
        return item
