# -*- coding: utf-8 -*-
import scrapy
from scrapy_redis.spiders import RedisSpider
from scrapy.exceptions import DontCloseSpider
from selenium import webdriver
import json
import time
import re
import math
import copy
import os

pwd = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
phantomjs_path = os.path.join(pwd, "phantomjs")
driver = webdriver.PhantomJS(executable_path=phantomjs_path)
driver.get("https://www.baidu.com/")

key_keyword = "change:keyword"
key_keyword_old = "change:keywordOld"


class ChangeSpider(RedisSpider):
    name = 'change'
    allowed_domains = ['baidu.com']

    def parse(self, response):
        meta = response.meta
        source = response.xpath("/html/body/script[4]/text()").extract_first()
        tkatrr = re.search(r"getAttribute\('(.*?)'\)", source).group(1)
        source = 'B=' + re.sub(r'\(function\(\).*\(\);', '', source)
        tk = re.compile(r'{}="(.*?)"'.format(tkatrr)).search(response.body.decode()).group(1)
        bid = response.xpath("//span[@id='baiducode']/text()").extract_first()
        driver.execute_script(source)
        tot = driver.execute_script("return B('{}','{}')".format(tk, bid))
        pid = re.search(r'"result":{"pid":"(.*?)"', response.body.decode()).group(1)
        totalPage = response.xpath(
            "//ul[@class='zx-detail-tab']/li[2]/span[2]/text()").extract_first()
        totalPage = totalPage.replace('+', '')
        totalPage = math.ceil(int(totalPage) / 5)

        for i in range(1, totalPage + 1):
            change_url = "https://xin.baidu.com/detail/changeajax?pid={}&tot={}&p={}&_={}".format(pid, tot, i,
                                                                                                  int(
                                                                                                      time.time() * 1000))
            meta_change = copy.deepcopy(meta)
            meta_change['page'], meta_change['ajaxType'] = 'p{}'.format(i), 'change'
            yield scrapy.Request(change_url, callback=self.parse_detail, priority=50, meta=meta_change)
        investPage = response.xpath(
            "//ul[@class='zx-detail-tab']/li[4]/span[2]/text()").extract_first()
        investPage = int(investPage.replace('+', ''))
        if investPage:
            invest_url = "https://xin.baidu.com/detail/investajax?pid={}&tot={}&p=1&_={}".format(pid, tot,
                                                                                                 int(
                                                                                                     time.time() * 1000))
            meta_invest = copy.deepcopy(meta)
            meta_invest['page'], meta_invest['ajaxType'] = 'p1', 'invest'
            branch_url = "https://xin.baidu.com/detail/branchajax?pid={}&tot={}&p=1&_={}".format(pid, tot,
                                                                                                 int(
                                                                                                     time.time() * 1000))
            meta_branch = copy.deepcopy(meta)
            meta_branch['page'], meta_branch['ajaxType'] = 'p1', 'branch'
            yield scrapy.Request(invest_url, callback=self.parse_list, priority=50, meta=meta_invest,dont_filter=True)
            yield scrapy.Request(branch_url, callback=self.parse_list, priority=50, meta=meta_branch,dont_filter=True)
        annualPage = response.xpath(
            "//ul[@class='zx-detail-tab']/li[5]/span[2]/text()").extract_first()
        annualPage = int(annualPage.replace('+', ''))
        if annualPage:
            annual_url = "https://xin.baidu.com/detail/annualajax?pid={}&year=&tot={}&_={}".format(pid, tot,
                                                                                                   int(
                                                                                                       time.time() * 1000))
            meta_annual = copy.deepcopy(meta)
            meta_annual['year'], meta_annual['ajaxType'] = 'y', 'annual'
            yield scrapy.Request(annual_url, callback=self.parse_list_nb, priority=50, meta=meta_annual,dont_filter=True)

    def parse_list(self, response):
        meta = response.meta
        item = json.loads(response.body.decode())
        if item['data']['totalNum'] != 0:
            pageCount = item['data']['pageCount']
            for i in range(1, pageCount + 1):
                url = re.sub(r'p=\d+', 'p={}'.format(i), response.url)
                meta_update = copy.deepcopy(meta)
                meta_update['page'] = 'p{}'.format(i)
                yield scrapy.Request(url, callback=self.parse_detail, priority=50, meta=meta_update)


    def parse_list_nb(self, response):
        meta = response.meta
        item = json.loads(response.body.decode())
        if item['data']['totalNum'] != 0:
            reportYears = item['data'].get('reportYears',[])
            for i in reportYears:
                url = re.sub(r'year=', 'year={}'.format(i), response.url)
                meta_update = copy.deepcopy(meta)
                meta_update['year'] = 'y{}'.format(i)
                yield scrapy.Request(url, callback=self.parse_detail, priority=50, meta=meta_update)


    def parse_detail(self, response):
        meta = response.meta
        item = json.loads(response.body.decode())
        item["data"]["referer"] = meta['url']
        item["data"]["entName"] = meta['comName']
        if 'investajax' in response.url:
            item['investajax'] = 'investajax'
            item['page'] = meta['page']
            item['type'] = meta['ajaxType']
        elif 'branchajax' in response.url:
            item['branchajax'] = 'branchajax'
            item['page'] = meta['page']
            item['type'] = meta['ajaxType']
        elif 'changeajax' in response.url:
            item['changeajax'] = 'changeajax'
            item['page'] = meta['page']
            item['type'] = meta['ajaxType']
        elif 'annualajax' in response.url:
            item['annualajax'] = 'annualajax'
            item['year'] = meta['year']
            item['type'] = meta['ajaxType']

        yield item

    def spider_idle(self):
        server = self.server
        for i in range(2000):
            keyword = server.blpop(key_keyword)[1].decode()
            sp_list = keyword.split(', ')
            url = sp_list[0]
            comName = sp_list[1]
            meta = {"url": url, 'comName': comName}
            req = scrapy.Request(url, callback=self.parse, dont_filter=True, meta=meta, priority=20)
            self.crawler.engine.crawl(req, spider=self)
        raise DontCloseSpider
