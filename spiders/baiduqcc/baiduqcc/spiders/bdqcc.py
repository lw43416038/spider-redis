# -*- coding: utf-8 -*-
import logging
import scrapy
from scrapy_redis.spiders import RedisSpider
from scrapy.exceptions import DontCloseSpider
from selenium import webdriver
import json
import time
import re
import os

pwd = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
phantomjs_path = os.path.join(pwd, "phantomjs")
driver = webdriver.PhantomJS(executable_path=phantomjs_path)
driver.get("https://www.baidu.com/")

key_keyword = "bdqcc:keyword"
key_keyword_old = "bdqcc:keywordOld"


class BdqccSpider(RedisSpider):
    name = 'bdqcc'
    allowed_domains = ['baidu.com']

    def parse_search(self, response):
        items = response.xpath("//*[@class='zx-list-item-url']")
        for i in items:
            comName = i.xpath("./@title").extract_first()
            href = i.xpath("./@href").extract_first()
            url = "https://xin.baidu.com" + href
            meta = {'comName': comName, 'url': url}
            yield scrapy.Request(url, callback=self.parse, meta=meta,priority=50)
        text = response.text
        temp = re.findall("totalPageNum\":(\d+)",text)
        if temp and int(temp[0]) > 1:
            totalPage = int(temp[0])
            for i in range(2,totalPage+1):
                keyword = response.meta["keyword"]
                url = "https://xin.baidu.com/s/l?q={keyword}&t=0&p={page}&s=10&o=0".format(keyword =keyword,page=i)
                yield scrapy.Request(url,callback=self.parse_search_no_page,dont_filter=True,priority=20)

    def parse_search_no_page(self, response):
        rj = json.loads(response.body.decode())
        resultList =rj["data"]["resultList"]
        for i in resultList:
            comName = i["titleName"]
            pid = i["pid"]
            url = "https://xin.baidu.com/detail/compinfo?pid=" + pid
            meta = {'comName': comName, 'url': url}
            yield scrapy.Request(url, callback=self.parse, meta=meta,priority=50)

    def parse(self, response):
        try:
            a_list = response.xpath("//ul[@class='zx-absorbed-list ZX_LOG_LINK']/li/a")
            for a in a_list:
                url = a.xpath("./@href").extract_first()
                url = response.urljoin(url)
                comName = a.xpath("./@title").extract_first()
                yield scrapy.Request(url, callback=self.parse, priority = 50,meta={"comName": comName, 'url': url})
            meta = response.meta
            source = response.xpath("/html/body/script[4]/text()").extract_first()
            tkatrr = re.search(r"getAttribute\('(.*?)'\)", source).group(1)
            source = 'B=' + re.sub(r'\(function\(\).*\(\);', '', source)
            tk = re.compile(r'{}="(.*?)"'.format(tkatrr)).search(response.body.decode()).group(1)
            bid = response.xpath("//span[@id='baiducode']/text()").extract_first()
            driver.execute_script(source)
            tot = driver.execute_script("return B('{}','{}')".format(tk, bid))
            pid = re.search(r'"result":{"pid":"(.*?)"', response.body.decode()).group(1)
            ajax_url = "https://xin.baidu.com/detail/basicAjax?pid={}&tot={}&_={}".format(pid, tot,
                                                                                          int(time.time() * 1000))
            yield scrapy.Request(ajax_url, callback=self.parse_detail, priority=100, meta=meta, dont_filter=True)
        except:
            request = response.request
            meta = request.meta
            retry = meta.get("retry",0) + 1
            self.log("retry : url <%s> num <%s> " % (response.url, retry), level=logging.INFO)
            if retry < 20:
                meta["retry"] = retry
                del meta["proxy"]
                retryreq = scrapy.Request(request.url,callback=self.parse,meta = meta,dont_filter=True,priority=50)
                yield retryreq


    def parse_detail(self, response):
        meta = response.meta
        item = json.loads(response.body.decode())
        item["data"]["referer"] = meta['url']
        return item

    def spider_idle(self):
        server = self.server
        for i in range(2000):
            keyword = server.spop(key_keyword)
            if keyword:
                keyword = keyword.decode()
                added = server.sadd(key_keyword_old, keyword)
                if added != 0:
                    url = "https://xin.baidu.com/s?q=" + keyword
                    meta = {"keyword":keyword}
                    req = scrapy.Request(url, callback=self.parse_search, dont_filter=True,meta=meta)
                    self.crawler.engine.crawl(req, spider=self)
            else:
                break
        raise DontCloseSpider
