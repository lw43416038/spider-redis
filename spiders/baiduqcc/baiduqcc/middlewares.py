# -*- coding: utf-8 -*-

# Define here the models for your spider middleware
#
# See documentation in:
# https://doc.scrapy.org/en/latest/topics/spider-middleware.html
from scrapy.http import Request
# from scrapy import signals
# from scrapy.utils.reqser import request_to_dict
# from scrapy_redis.picklecompat import dumps
from scrapy.exceptions import IgnoreRequest
import json


key = "bdqcc:proxy"
key_error = "bdqcc:error"

class BaiduqccSpiderMiddleware1(object):
    def process_request(self, request, spider):
        if request.url.startswith("https://xin.baidu.com/fs/check"):
            raise IgnoreRequest
        if 'proxy' not in request.meta:
            proxy = spider.server.srandmember(key).decode()
            request.meta["proxy"] = "http://" + proxy

    def process_response(self,request,response,spider):
        if response.url.startswith("https://xin.baidu.com/fs/check"):
            # request = response.request
            meta = request.meta
            if "proxy" in meta:
                del meta["proxy"]
            request_new = Request(request.url,callback=request.callback,meta =meta,priority=request.priority,dont_filter=True)
            return request_new
        else:
            return response

    def process_exception(self, request, exception, spider):
        meta = request.meta
        if "retry" in meta:
            retry = meta["retry"]
            retry += 1
        else:
            retry = 1
        if retry < 5:
            if "url" in meta:
                url = meta["url"]
                cb = spider.parse
            else:
                url = request.url
                cb = request.callback
            if "proxy" in meta:
                del meta["proxy"]
            meta["retry"] = retry
            request_new = Request(url, meta=meta,priority=50, callback=cb,dont_filter=True)
            return request_new
        else:
            server = spider.server
            errorInfo = json.dumps(request.url)
            server.sadd(key_error,errorInfo)


# class BaiduqccSpiderMiddleware(object):
#
#
#     def process_spider_exception(self, response, exception, spider):
#         request = response.request
#         meta = request.meta
#         # if "comName" in meta:
#         if "retry" in meta:
#             retry = meta["retry"]
#             retry += 1
#         else:
#             retry = 1
#         if retry < 5:
#             if "url" in meta:
#                 url = meta["url"]
#                 cb = spider.parse
#                 if "proxy" in meta:
#                     del meta["proxy"]
#                 meta["retry"] = retry
#                 request_new = Request(url, meta=meta,priority=50, callback=cb,dont_filter=True)
#                 return request_new
#         else:
#             server = spider.server
#             errorInfo = json.dumps(request.url)
#             server.sadd(key_error,errorInfo)

