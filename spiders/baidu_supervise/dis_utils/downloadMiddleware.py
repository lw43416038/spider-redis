import six
from scrapy import Request
from six.moves.urllib.parse import urljoin
from w3lib.url import safe_url_string
from scrapy.downloadermiddlewares.retry import RetryMiddleware
from scrapy.downloadermiddlewares.redirect import BaseRedirectMiddleware
from redis import Redis


r = Redis(host='10.28.162.198', port=6381, password='yWY0a7fKB3K1pTFo', db=3)

key = "bdqcc:proxy"
basic_priority = 50
retry_callback_method = "parse"


def request_retry(request, spider):
    # retryreq = request.copy()

    meta = request.meta
    if 'url' in meta:
        url = meta["url"]
        priority = request.priority + 1
        callback = getattr(spider, retry_callback_method)
    else:
        url = request.url
        priority = request.priority
        callback = request.callback

    retries = meta.get('retry_times', 0) + 1
    meta["retry_times"] = retries

    if "proxy" in meta:
        del meta["proxy"]
    retryreq = Request(url, callback=callback, meta=meta, priority=priority, dont_filter=True)

    return retryreq


class Retry(RetryMiddleware):

    def _retry(self, request, reason, spider):
        retries = request.meta.get('retry_times', 0) + 1
        retry_times = self.max_retry_times
        if retries <= retry_times:
            retryreq = request_retry(request, spider)
            return retryreq



class Proxy():
    def process_request(self, request, spider):
        if 'proxy' not in request.meta:
            proxy = r.srandmember(key).decode()
            request.meta["proxy"] = "http://" + proxy


class Redirect(BaseRedirectMiddleware):

    def process_response(self, request, response, spider):

        if (request.meta.get('dont_redirect', False) or
                response.status in getattr(spider, 'handle_httpstatus_list', []) or
                response.status in request.meta.get('handle_httpstatus_list', []) or
                request.meta.get('handle_httpstatus_all', False)):
            return response

        allowed_status = (301, 302, 303, 307, 308)
        if 'Location' not in response.headers or response.status not in allowed_status:
            return response

        location = safe_url_string(response.headers['location'])
        redirected_url = urljoin(request.url, location)
        # have verifyCode
        if redirected_url.startswith("https://xin.baidu.com/fs/check"):
            return request_retry(request, spider)
        redirected = self._redirect_request_using_get(request, redirected_url)
        return self._redirect(redirected, request, spider, response.status)
