import os

LOG_DIR = "/var/log/docker"
os.path.exists(LOG_DIR) or os.mkdir(LOG_DIR)
LOG_FILE = os.path.join(LOG_DIR, "supervise.log")
LOG_LEVEL = "INFO"

DOWNLOAD_TIMEOUT = 10

RETRY_TIMES = 20

DOWNLOADER_MIDDLEWARES = {
    'scrapy.downloadermiddlewares.retry.RetryMiddleware': None,
    'dis_utils.downloadMiddleware.Retry': 543,
    'dis_utils.downloadMiddleware.Proxy': 555,
    'dis_utils.downloadMiddleware.Redirect': 600,
    'scrapy.downloadermiddlewares.redirect.RedirectMiddleware': None,
    'scrapy.downloadermiddlewares.stats.DownloaderStats': 850,
}

# SPIDER_MIDDLEWARES = {
#     'dis_utils.spiderMiddleware.Retry': 950
# }
