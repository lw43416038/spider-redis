# -*- coding: utf-8 -*-
import scrapy
from scrapy_redis.spiders import RedisSpider
from scrapy.exceptions import DontCloseSpider
from selenium import webdriver
from selenium.webdriver.common.desired_capabilities import DesiredCapabilities
import json
import time
import re
import copy
import os

pwd = os.path.dirname(os.path.dirname(os.path.dirname(os.path.abspath(__file__))))
phantomjs_path = os.path.join(pwd, "phantomjs")
dcap = dict(DesiredCapabilities.PHANTOMJS)
dcap["phantomjs.page.settings.userAgent"] = ("Mozilla/5.0 (Windows NT 6.1) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/49.0.2623.221 Safari/537.36 SE 2.X MetaSr 1.0")
driver = webdriver.PhantomJS(executable_path=phantomjs_path,desired_capabilities=dcap)
driver.get("https://www.baidu.com/")

key_keyword = "supervise:keyword"


class SuperviseSpider(RedisSpider):
    name = 'supervise'
    allowed_domains = ['baidu.com']

    def parse(self, response):
        try:
            meta = response.meta
            source = response.xpath("/html/body/script[4]/text()").extract_first()
            tkatrr = re.search(r"getAttribute\('(.*?)'\)", source).group(1)
            source = 'B=' + re.sub(r'\(function\(\).*\(\);', '', source)
            tk = re.compile(r'{}="(.*?)"'.format(tkatrr)).search(response.body.decode()).group(1)
            bid = response.xpath("//span[@id='baiducode']/text()").extract_first()
            try:
                driver.execute_script(source)
            except:
                pass
            tot = driver.execute_script("return B('{}','{}')".format(tk, bid))
            pid = re.search(r'"result":{"pid":"(.*?)"', response.body.decode()).group(1)
            discreditPage = response.xpath(
                "//ul[@class='zx-detail-tab']/li[6]/span[2]/text()").extract_first()
            discreditPage = int(discreditPage.replace('+', ''))
            if discreditPage:
                discredit_url = 'https://xin.baidu.com/detail/discreditAjax?pid={}&p=1&tot={}&_={}'.format(pid, tot, int(
                    time.time() * 1000))
                meta_discredit = copy.deepcopy(meta)
                meta_discredit['page'], meta_discredit['ajaxType'] = 'p1', 'discredit'

                abnormal_url = 'https://xin.baidu.com/detail/abnormalAjax?pid={}&p=1&tot={}&_={}'.format(pid, tot, int(
                    time.time() * 1000))
                meta_abnormal = copy.deepcopy(meta)
                meta_abnormal['page'], meta_abnormal['ajaxType'] = 'p1', 'abnormal'

                lawWenshu_url = 'https://xin.baidu.com/detail/lawWenshuAjax?pid={}&p=1&tot={}&_={}'.format(pid, tot, int(
                    time.time() * 1000))
                meta_lawWenshu = copy.deepcopy(meta)
                meta_lawWenshu['page'], meta_lawWenshu['ajaxType'] = 'p1', 'lawWenshu'

                quality_url = 'https://xin.baidu.com/detail/qualityAjax?pid={}&p=1&tot={}&_={}'.format(pid, tot, int(
                    time.time() * 1000))
                meta_quality = copy.deepcopy(meta)
                meta_quality['page'], meta_quality['ajaxType'] = 'p1', 'quality'

                foodquality_url = 'https://xin.baidu.com/detail/foodqualityajax?pid={}&p=1&tot={}&_={}'.format(pid, tot,
                                                                                                               int(
                                                                                                                   time.time() * 1000))
                meta_foodquality = copy.deepcopy(meta)
                meta_foodquality['page'], meta_foodquality['ajaxType'] = 'p1', 'foodquality'

                penalties_url = 'https://xin.baidu.com/detail/penaltiesAjax?pid={}&p=1&tot={}&_={}'.format(pid, tot, int(
                    time.time() * 1000))
                meta_penalties = copy.deepcopy(meta)
                meta_penalties['page'], meta_penalties['ajaxType'] = 'p1', 'penalties'

                yield scrapy.Request(discredit_url, callback=self.parse_list, priority=50, meta=meta_discredit,
                                     dont_filter=True)
                yield scrapy.Request(abnormal_url, callback=self.parse_list, priority=50, meta=meta_abnormal,
                                     dont_filter=True)
                yield scrapy.Request(lawWenshu_url, callback=self.parse_list, priority=50, meta=meta_lawWenshu,
                                     dont_filter=True)
                yield scrapy.Request(quality_url, callback=self.parse_list, priority=50, meta=meta_quality,
                                     dont_filter=True)
                yield scrapy.Request(foodquality_url, callback=self.parse_list, priority=50, meta=meta_foodquality,
                                     dont_filter=True)
                yield scrapy.Request(penalties_url, callback=self.parse_list, priority=50, meta=meta_penalties,
                                     dont_filter=True)
        except:
            request = response.request
            meta = request.meta
            parseRetry = meta.get("parseRetry", 0) + 1
            self.log("parseRetry : url <%s> num <%s> " % (response.url, parseRetry))
            if parseRetry < 5:
                meta["parseRetry"] = parseRetry
                del meta["proxy"]
                retryreq = scrapy.Request(request.url, callback=self.parse, meta=meta, dont_filter=True, priority=50)
                yield retryreq

    def parse_list(self, response):
        meta = response.meta
        item = json.loads(response.body.decode())
        pageCount = item['data']['pageCount']
        if pageCount != 0:
            for i in range(1, pageCount + 1):
                url = re.sub(r'p=\d+', 'p={}'.format(i), response.url)
                meta_update = copy.deepcopy(meta)
                meta_update['page'] = 'p{}'.format(i)
                yield scrapy.Request(url, callback=self.parse_detail, priority=50, meta=meta_update)

    def parse_detail(self, response):
        meta = response.meta
        item = json.loads(response.body.decode())
        item["data"]["referer"] = meta['url']
        item["data"]["entName"] = meta['comName']
        item['page'] = meta['page']
        item['type'] = meta['ajaxType']
        if 'discreditAjax' in response.url:
            item['discreditAjax'] = 'discreditAjax'
            self.add_fingerprint(item)
            yield item
        if 'abnormalAjax' in response.url:
            item['abnormalAjax'] = 'abnormalAjax'
            self.add_fingerprint(item)
            yield item
        if 'qualityAjax' in response.url:
            item['qualityAjax'] = 'qualityAjax'
            self.add_fingerprint(item)
            yield item
        if 'foodqualityajax' in response.url:
            item['foodqualityajax'] = 'foodqualityajax'
            self.add_fingerprint(item)
            yield item
        if 'penaltiesAjax' in response.url:
            item['penaltiesAjax'] = 'penaltiesAjax'
            self.add_fingerprint(item)
            yield item
        if 'lawWenshuAjax' in response.url:
            self.add_fingerprint(item)
            for i in item['data']['list']:
                wenshuId = i['wenshuId']
                type = i['type']
                role = i['role']
                caseNo = i['caseNo']
                procedure = i['procedure']
                meta_lawWenshu = {'type': type, 'entName': meta['comName'], 'role': role,
                                  'caseNo': caseNo, 'procedure': procedure}
                url = 'https://xin.baidu.com/detail/wenshuAjax?wenshuId={}&_={}'.format(wenshuId,
                                                                                        int(time.time() * 1000))
                yield scrapy.Request(url, callback=self.parse_result, priority=70, dont_filter=True,
                                     meta=meta_lawWenshu)

    def parse_result(self, response):
        meta = response.meta
        item = json.loads(response.body.decode())
        item['data']['type'] = meta.get('type')
        item['data']['entName'] = meta.get('entName')
        item['data']['role'] = meta.get('role')
        item['data']['caseNo'] = meta.get('caseNo')
        item['data']['procedure'] = meta.get('procedure')
        item['lawWenshuAjax'] = 'lawWenshuAjax'
        yield item

    def add_fingerprint(self, item):
        name = item["data"]["entName"]
        key = self.name + ":dupefilter" + ":" + name + ":" + "crawled"
        server = self.server
        page = item.get('page', '')
        type = item.get('type', '')
        value = type + " " + page
        server.sadd(key, value)
        server.expire(key, 10 * 60)

    def spider_idle(self):
        server = self.server
        for i in range(2000):
            keyword = server.blpop(key_keyword)[1].decode()
            sp_list = keyword.split(', ')
            url = sp_list[0]
            comName = sp_list[1]
            meta = {"url": url, 'comName': comName}
            req = scrapy.Request(url, callback=self.parse, dont_filter=True, meta=meta, priority=20)
            self.crawler.engine.crawl(req, spider=self)
        raise DontCloseSpider
