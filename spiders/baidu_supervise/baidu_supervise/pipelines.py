# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import json

class BaiduSupervisePipeline(object):

    def process_item(self, item, spider):
        data = json.dumps(item, ensure_ascii=False)

        if 'discreditAjax' in item:
            spider.server.rpush("discredit:data", data)
        elif 'abnormalAjax' in item:
            spider.server.rpush("abnormal:data", data)
        elif 'lawWenshuAjax' in item:
            spider.server.rpush("lawWenshu:data", data)
        elif 'qualityAjax' in item:
            spider.server.rpush("quality:data", data)
        elif 'foodqualityajax' in item:
            spider.server.rpush("foodquality:data", data)
        elif 'penaltiesAjax' in item:
            spider.server.rpush("penalties:data", data)

        return item
