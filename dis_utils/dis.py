import os
import sys
import shutil
from importlib import import_module
from configparser import ConfigParser
from scrapy import Spider


dis_utils_name = "dis_utils"
dis_spiders_dir = "dis_spiders"
dis_setting_name = "dis_setting"
dis_setting_py = dis_setting_name + ".py"
spiders_scrapy_dis_dir = "spiders_dis"

new_spider_code = """
import os
from %(project_spider_package_setting)s.%(module)s import %(cls)s
from scrapy_redis.spiders import RedisSpider

class %(cls)sDis(RedisSpider,%(cls)s):    
    LOG_DIR = "/var/log/docker"
    os.path.exists(LOG_DIR) or os.mkdir(LOG_DIR)
    LOG_FILE = os.path.join(LOG_DIR,%(cls)s.name + ".log")
    custom_settings = {
        "LOG_FILE":LOG_FILE
    }
"""
new_spider_code_scrapy_redis = """
import os
from %(project_spider_package_setting)s.%(module)s import %(cls)s

class %(cls)sDis(%(cls)s):    
    LOG_DIR = "/var/log/docker"
    os.path.exists(LOG_DIR) or os.mkdir(LOG_DIR)
    LOG_FILE = os.path.join(LOG_DIR,%(cls)s.name + ".log")
    LOG_LEVEL = "INFO"
    custom_settings = {
        "LOG_FILE":LOG_FILE,
        "LOG_LEVEL":LOG_LEVEL
    }
"""



def spider_to_dis(project,is_scrapy_redis = False):
    pwd = os.getcwd()
    project_dir_old = os.path.join(pwd, "spiders", project)
    project_dir = os.path.join(pwd, spiders_scrapy_dis_dir, project)
    sys.path.append(project_dir)
    assert os.path.exists(project_dir_old)
    if os.path.exists(project_dir):
        del_flag = input("project_dir <%s> existed , do yo want remove,Y or N "%project_dir)
        if del_flag in ["y", "Y"]:
            shutil.rmtree(project_dir)
        else:
            print("==========end=========")
            sys.argv(0)
    shutil.copytree(project_dir_old, project_dir)
    print("copy {old} -> {dis}".format(old=project_dir_old, dis=project_dir))

    # 获取scrapy 项目配置，setting path
    print("add dis_utils setting")
    spider_cfg_file = os.path.join(project_dir, "scrapy.cfg")
    cf = ConfigParser()
    cf.read(spider_cfg_file)
    spider_setting_default = cf.get("settings", "default")

    temp = spider_setting_default.split(".")
    projcet_setting_path_list = [spiders_scrapy_dis_dir, project, spider_setting_default]
    projcet_setting_path = ".".join(projcet_setting_path_list)
    spiders_package= getattr(import_module(projcet_setting_path), "NEWSPIDER_MODULE")
    project_spiders_old = os.path.join(project_dir, *spiders_package.split("."))

    # 查找相关爬虫
    spiders_old_package_path = ".".join([spiders_scrapy_dis_dir, project, spiders_package])
    spiders_list = list()
    spiders_cls_list = list()
    for i in os.listdir(project_spiders_old):
        if i.endswith(".py") and i != "__init__.py":
            spider_module_name = i.rstrip(".py")
            spider_module = import_module(spiders_old_package_path + "." + spider_module_name)
            for j in dir(spider_module):
                cls = getattr(spider_module, j)
                if isinstance(cls, type) and issubclass(cls, Spider) and cls.name:
                    spiders_list.append([spider_module_name, cls.__name__])
                    spiders_cls_list.append(cls)

    # 将需要添加的配置文件复制到项目下面的dis_utils_name文件夹中
    dis_src = os.path.join(pwd,"ext",project)
    if os.path.exists(dis_src):
        dis_dst = os.path.join(project_dir, dis_utils_name)
        os.path.exists(dis_dst) or os.mkdir(dis_dst)
        copytree2(dis_src, dis_dst)
        # 将dis需要添加的配置写入setting
        spider_setting_file = os.path.join(project_dir,*temp)
        with open(spider_setting_file + ".py" ,"a+",encoding="utf8") as f:
            package = ".".join([dis_utils_name,dis_setting_name])
            f.write("from {package} import * ".format(package = package))
            f.write("\n")
    if is_scrapy_redis == False:
        # 改变spider 使用scrapy_redis
        print("create dis_utils spider")
        project_spiders_dis = os.path.join(project_dir, dis_spiders_dir)
        # shutil.copytree(project_spiders_old,project_spiders_dis)
        os.path.exists(project_spiders_dis) or os.mkdir(project_spiders_dis)
        #删除以前的文件
        for spider_module_name,clsName in spiders_list:
            file_path = os.path.join(project_spiders_dis,spider_module_name + ".py")
            if os.path.exists(file_path):
                os.remove(file_path)
        #写入新的爬虫代码
        for spider_module_name,clsName in spiders_list:
            file_path = os.path.join(project_spiders_dis,spider_module_name + ".py")
            with open(file_path,"a+",encoding="utf8") as f:
                f.write(new_spider_code % dict(project_spider_package_setting = spiders_package,
                                               module = spider_module_name,
                                               cls = clsName
                                               ))

    print("+++++++++make dis_utils spider finish ++++++++++++++++====")
    #创建启动文件
    start_file = os.path.join(pwd, dis_utils_name,"start.cfg")
    cf = ConfigParser()
    cf.read(start_file)
    for cls in spiders_cls_list:
        module = cls.__module__
        clsname = cls.__name__
        spider = cls.name
        if spider not in cf.sections():
            cf.add_section(spider)
        cf.set(spider,"module",module)
        cf.set(spider, "clsname", clsname)
        cf.set(spider,"setting",projcet_setting_path)
        make_dockerfile(project_path=project_dir, spider = spider)
        print("======make docker files for {spider}======".format(spider =spider))
    with open(start_file,"w") as f:
        cf.write(f)
    print("===== set start config finished=========")

def copytree2(src,dst):  #覆盖式复制文件夹
    from os.path import exists,join,isdir
    exists(dst) or os.mkdir(dst)
    for c1 in os.listdir(src):
        c1_p = join(src,c1)
        c1_p_dst = join(dst, c1)
        if isdir(c1_p):
            copytree2(c1_p,c1_p_dst)
        else:
            try:
                shutil.copy2(c1_p,c1_p_dst)
            except Exception as e:
                print(e)

def update(project,is_scrapy_redis = False):
    pwd = os.getcwd()
    project_dir_old = os.path.join(pwd, "spiders", project)
    project_dir = os.path.join(pwd, spiders_scrapy_dis_dir, project)
    sys.path.append(project_dir)
    assert os.path.exists(project_dir_old)
    if not os.path.exists(project_dir):
        del_flag = input("project_dir <%s> dont existed , do yo want make,Y or N "%project_dir)
        if del_flag in ["y", "Y"]:
            # shutil.rmtree(project_dir)
            spider_to_dis(project,is_scrapy_redis)
        else:
            print("+++++++end+++++++++++++++")
            sys.argv(0)
    copytree2(project_dir_old,project_dir)
    print("add dis_utils setting")
    dis_src = os.path.join(pwd,"ext",project)
    if os.path.exists(dis_src):
        dis_dst = os.path.join(project_dir, dis_utils_name)
        os.path.exists(dis_dst) or os.mkdir(dis_dst)
        copytree2(dis_src, dis_dst)
        print("dis_utils src <%s> -> dst <%s>"%(dis_src,dis_dst))
        spider_cfg_file = os.path.join(project_dir, "scrapy.cfg")
        cf = ConfigParser()
        cf.read(spider_cfg_file)
        spider_setting_default = cf.get("settings", "default")
        temp = spider_setting_default.split(".")
        spider_setting_file = os.path.join(project_dir, *temp)
        with open(spider_setting_file + ".py", "a+", encoding="utf8") as f:
            package = ".".join([dis_utils_name, dis_setting_name])
            f.write("from {package} import * ".format(package=package))
            f.write("\n")
        print("+++++++end+++++++++++++++")

def make_dockerfile(project_path,spider):
    basic_build = """FROM docker:5000/scrapy:redis

WORKDIR /spider

COPY ./requirements.txt /spider/requirements.txt

RUN pip install --trusted-host pypi.python.org -r requirements.txt

CMD ["scrapy", "crawl","{spider}"]

    """
    basic_update = """FROM {spider}:basic
COPY . /spider
RUN pip install --trusted-host pypi.python.org -r requirements.txt
"""
    sh_build = """#! /bin/bash
docker build -t {spider}:basic -f dockerfile_build {project_path}
"""
    sh_update = """#! /bin/bash
docker build -t {spider}:stable -f dockerfile_update {project_path}  
"""
    sh_push = """#! /bin/bash
docker tag {spider}:stable docker:5000/{spider}:stable
docker push docker:5000/{spider}:stable
    """
    pwd = os.getcwd()
    docker_dir = os.path.join(pwd,"docker",spider)
    os.path.exists(docker_dir) or os.mkdir(docker_dir)
    dockerfile_build = os.path.join(pwd,"docker",spider,"dockerfile_build")
    dockerfile_update = os.path.join(pwd, "docker",spider, "dockerfile_update")
    sh_build_file = os.path.join(pwd, "docker",spider, "build.sh")
    sh_update_file = os.path.join(pwd, "docker",spider, "update.sh")
    sh_push_file = os.path.join(pwd, "docker", spider, "push.sh")
    cfg_build = basic_build.format(spider =spider)
    with open(dockerfile_build,"w") as f:
        f.write(cfg_build)
    cfg_update = basic_update.format(spider=spider)
    with open(dockerfile_update, "w") as f:
        f.write(cfg_update)
    sh_build = sh_build.format(project_path =project_path,spider =spider)
    sh_update = sh_update.format(project_path=project_path, spider=spider)
    sh_push = sh_push.format(spider = spider)
    with open(sh_build_file,"w") as f:
        f.write(sh_build)
    with open(sh_update_file,"w") as f:
        f.write(sh_update)
    with open(sh_push_file,"w") as f:
        f.write(sh_push)
    os.popen("pipreqs %s --force"%project_path)
