import os

SPIDER_MODULES = ['dis_spiders']

NEWSPIDER_MODULE = 'dis_spiders'

# LOG_LEVEL = "INFO"

ROBOTSTXT_OBEY = False

# Configure maximum concurrent requests performed by Scrapy (default: 16)
CONCURRENT_REQUESTS = 128

# The download delay setting will honor only one of:
CONCURRENT_REQUESTS_PER_DOMAIN = 32

SCHEDULER = "scrapy_redis.scheduler.Scheduler"
SCHEDULER_IDLE_BEFORE_CLOSE = 10
DUPEFILTER_CLASS = "scrapy_redis.dupefilter.RFPDupeFilter"

#redis config
REDIS_CONFIG = {
    "host": "10.81.128.205",
    # "host" : "47.100.77.146",
    "port": 6379,
    "password": "TX6cVsKrMMUfYptmO+fnD5abN2grDRaM",
    "db": 1
}

REDIS_URL = 'redis://%s:%s@%s:%s?db=%s'%(REDIS_CONFIG["host"],REDIS_CONFIG["password"],REDIS_CONFIG["host"],REDIS_CONFIG["port"],REDIS_CONFIG["db"])
REDIS_START_URLS_KEY = '%(name)s:start_urls'
