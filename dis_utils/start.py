import os
from redis import Redis
# from .dis_setting import REDIS_CONFIG
from scrapy_redis import picklecompat,defaults
from scrapy_redis.queue import PriorityQueue
from scrapy_redis.defaults import SCHEDULER_QUEUE_KEY,SCHEDULER_DUPEFILTER_KEY,SCHEDULER_DUPEFILTER_CLASS
from scrapy.utils.reqser import request_from_dict
from scrapy.utils.misc import load_object
from configparser import ConfigParser
pwd = os.getcwd()

from .dis import dis_utils_name

REDIS_CONFIG = {
    # "host": "10.81.128.205",
    "host" : "47.100.77.146",
    "port": 6379,
    "password": "TX6cVsKrMMUfYptmO+fnD5abN2grDRaM",
    "db": 1
}


def main(spider,dont_filter = False,flush = False):
    """
    :param spider:  爬虫名
    :param dont_filter:  是否去重
    :param flush:  是否clear缓存request
    :return:
    """
    start_file = os.path.join(pwd, dis_utils_name, "start.cfg")
    cf = ConfigParser()
    cf.read(start_file)
    module = cf.get(spider,"module")
    clsname = cf.get(spider,"clsname")
    # setting_path = cf.get(spider,"setting")
    cls = load_object(".".join([module,clsname]))
    # setting = load_object(setting_path)
    spider_obj = cls()
    requests = spider_obj.start_requests()
    server = Redis(**REDIS_CONFIG)
    q = PriorityQueue(server, spider_obj, SCHEDULER_QUEUE_KEY)
    dq = load_object(SCHEDULER_DUPEFILTER_CLASS)(server, SCHEDULER_DUPEFILTER_KEY % {'spider': spider_obj.name})
    if flush:
        q.clear()
        dq.clear()
    for request in requests:
        if dont_filter or not dq.request_seen(request):
            q.push(request)
        # dq.request_seen(request)
        # break
