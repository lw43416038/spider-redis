#coding:utf8
from sqlalchemy import create_engine
from sqlalchemy.orm import sessionmaker
from sqlalchemy.orm.session import Session
from db.models import ComDB,SJZHDB,SpiderDB
from db.setting import DB_CONFIG,DB_CONFIG_COM

engine_data = create_engine("mysql+pymysql://{user}:{passwd}@{host}:{port}/{db}?charset={charset}".format(**DB_CONFIG["data"]),pool_recycle = 7200)
# Session_Data = sessionmaker(engine_data)

engine_spider = create_engine("mysql+pymysql://{user}:{passwd}@{host}:{port}/{db}?charset={charset}".format(**DB_CONFIG["spider"]),pool_recycle = 7200)
# Session_Spider = sessionmaker(engine_spider)

engine_log = create_engine("mysql+pymysql://{user}:{passwd}@{host}:{port}/{db}?charset={charset}".format(**DB_CONFIG["log"]),pool_recycle = 7200)
# Session_Log = sessionmaker(engine_log)

engine_com = create_engine("mysql+pymysql://{user}:{passwd}@{host}:{port}/{db}?charset={charset}".format(**DB_CONFIG_COM),pool_recycle = 7200)
# Session_Com = sessionmaker(engine_com)

engine_SJZH = create_engine("mysql+pymysql://{user}:{passwd}@{host}:{port}/{db}?charset={charset}".format(**DB_CONFIG["SJZH"]),pool_recycle = 7200)
# Session_SJZH = sessionmaker(engine_SJZH)

class SessionDB(Session):
    def get_bind(self, mapper=None, clause=None):
        if self.bind:
            return self.bind
        cls = mapper.entity()
        if isinstance(cls,SpiderDB):
            return engine_spider
        if isinstance(cls,SJZHDB):
            return engine_SJZH
        if isinstance(cls,ComDB):
            return engine_com
        return engine_data

Session_Factory = sessionmaker(class_=SessionDB)

