# -*- coding: utf-8 -*-
from redis import Redis
from .setting import REDIS_CONFIG

r = Redis(**REDIS_CONFIG)

class RedisUtils():
    """
    redis操作公共类
    """

    def set(self, key, value,**kwargs):
        r.set(key, value,**kwargs)

    def get(self, key):
        return r.get(key).decode("utf8")

    def expire(self, key, value):
        
        r.expire(key, value)

    def hkeys(self,name):
        return r.hkeys(name)


    def delete(self, key):
        
        r.delete(key)

    def rpush(self, key, value):
        
        r.rpush(key, value)

    def llen(self, key):
        
        return r.llen(key)

    def lrange(self, key, start, end):
        
        return r.lrange(key, start, end)

    def blpop(self, key, t):
        
        return r.blpop(key, t)

    def getStateTTL(self, key):
        ttl = r.ttl(key)
        if ttl== None or ttl == -2:
            ttl= 0
        return ttl

    def ttl(self,key):
        return r.ttl(key)

    def getAllSortSetEle(self, key):
        
        return r.zrange(key, 0, 10000)

    def getAllSetEle(self, key):
        
        return r.smembers(key)

    def zadd(self, key, *args):
        
        r.zadd(key,*args)

    def zrange(self, key, start, stop, withscores=False):
        
        return r.zrange(key, start, stop, withscores=withscores)

    def exists(self, key):
        
        return r.exists(key)

    def saddSet(self, key, ele):
        
        r.sadd(key, ele)

    def isMembers(self, set, ele):
        
        return r.sismember(set, ele)

    def srem(self, set, ele):
        
        r.srem(set, ele)

    def zrem(self, set, ele):
        
        r.zrem(set, ele)

    def isMembersInSortSet(self, zset, ele):
        
        if r.zrank(zset, ele) is not None:
            return True
        else:
            return False

    def zincrby(self, zset, ele, amount):
        
        r.zincrby(zset, ele, amount)

    def zscore(self, zset, ele):
        
        return r.zscore(zset, ele)

    def diffSet(self, key):
        
        return r.sdiff(key)

    def incr(self, key, amount):
        
        r.incr(key, amount)

    def hset(self, name, key, val):
        
        r.hset(name, key, val)

    def hget(self, name, key):
        result = r.hget(name, key)
        if result:
            return result.decode("utf8")
        else:
            return ""
    def keys(self, pattern):

        return r.keys(pattern)
    def flushall(self):
        r.flushall()
    def hdel(self,name,key):
        r.hdel(name,key)

    def execute_command(self,*args,**kwargs):
        r.execute_command(*args,**kwargs)
