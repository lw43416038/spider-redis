
import os
from weather.spiders.weather import WeatherSpider
from scrapy_redis.spiders import RedisSpider

class WeatherSpiderDis(RedisSpider,WeatherSpider):    
    LOG_DIR = "/var/log/docker"
    os.path.exists(LOG_DIR) or os.mkdir(LOG_DIR)
    LOG_FILE = os.path.join(LOG_DIR,WeatherSpider.name + ".log")
    custom_settings = {
        "LOG_FILE":LOG_FILE
    }
