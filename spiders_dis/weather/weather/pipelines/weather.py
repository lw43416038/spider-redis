# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
from db.sessions import Session_Factory
from db.models import Weather

class ScarpyDisPipeline(object):
    def process_item(self, item, spider):
        return item




class WeatherPipeline(object):


    def process_item(self,item,spider):
        # pass
        # print("=="*20)
        session = Session_Factory()
        session.add(Weather(**item))
        session.commit()
        session.close()