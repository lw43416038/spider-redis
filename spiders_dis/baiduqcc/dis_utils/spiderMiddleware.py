
import logging
from .downloadMiddleware import request_retry
from json.decoder import JSONDecodeError
logger = logging.getLogger(__name__)

class Retry(object):

    def __init__(self,settings):
        pass


    @classmethod
    def from_crawler(cls,crawler):
        settings = crawler.settings
        return cls(settings)


    def process_spider_exception(self,response, exception, spider):
        if isinstance(exception,(JSONDecodeError,AttributeError)):
            try:
                request = response.request
                retryreq = request_retry(request,spider)
                logger.info("retry: url <%s> "%retryreq.url)
                yield retryreq
            except:
                logger.exception("unknown error")







