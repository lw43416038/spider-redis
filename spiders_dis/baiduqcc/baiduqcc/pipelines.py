# -*- coding: utf-8 -*-

# Define your item pipelines here
#
# Don't forget to add your pipeline to the ITEM_PIPELINES setting
# See: https://doc.scrapy.org/en/latest/topics/item-pipeline.html
import json
# from redis import Redis
key_crawling = "bdqcc:crawling"

class BaiduqccPipeline(object):

    def process_item(self, item, spider):
        self.r = spider.server
        item = json.dumps(item, ensure_ascii=False)
        self.r.rpush("bdqcc:data", item)
        return item
