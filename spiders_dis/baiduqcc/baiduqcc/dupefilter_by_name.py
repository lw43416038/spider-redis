from redis import StrictRedis
from hashlib import md5
from scrapy_redis.dupefilter import RFPDupeFilter

key_crawling = "bdqcc:crawling"
redis_config = {
    "host": "10.81.129.100",
    "port": 6379,
    "password": "TX6cVsKrMMUfYptmO+fnD5abN2grDRaM",
    "db": 3
}
server2 = StrictRedis(**redis_config)

def get_m5(text):
    m5 = md5()
    m5.update(text.encode())
    return m5.hexdigest()[:16]


class RFPDupeFilter_name(RFPDupeFilter):

    def request_seen(self, request):
        fp = self.request_fingerprint(request)
        # This returns the number of values added, zero if already exists.
        m = int(fp[0], 16)
        if m % 2 == 0:
            server = self.server
        else:
            server = server2
        added = server.sadd(self.key, fp)
        return added == 0

    def request_fingerprint(self, request):
        name = request.meta["comName"]
        return get_m5(name)
