
import logging
from scrapy.downloadermiddlewares.retry import RetryMiddleware

logger = logging.getLogger(__name__)

class Retry(RetryMiddleware):

    def _retry(self, request, reason, spider):

        retries = request.meta.get('retry_times', 0) + 1

        retry_times = self.max_retry_times

        if 'max_retry_times' in request.meta:
            retry_times = request.meta['max_retry_times']

        if retries <= retry_times:
            logger.debug("Retrying %(request)s (failed %(retries)d times): %(reason)s",
                         {'request': request, 'retries': retries, 'reason': reason},
                         extra={'spider': spider})
            retryreq = request.copy()
            retryreq.meta['retry_times'] = retries
            del retryreq.meta["proxy"]
            retryreq.dont_filter = True
            retryreq.priority = request.priority

            return retryreq


class Proxy():
    def __init__(self,key):
        self.key = key

    @classmethod
    def from_crawler(cls,crawler):
        settings = crawler.settings
        key =settings.get("REDIS_PROXY_KEY","tax:proxy")
        return cls(key)

    def process_request(self,request,spider):
        if 'proxy' not in request.meta:
            proxy = spider.server.srandmember(self.key).decode()
            # proxy = "183.166.6.217:44658"
            request.meta["proxy"] = "http://" + proxy


