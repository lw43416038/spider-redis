
import logging
from scrapy import Request
from scrapy_redis.spiders import RedisSpider
from scrapy.exceptions import DontCloseSpider

logger = logging.getLogger(__name__)




class Tax(RedisSpider):

    name = "tax"
    redis_batch_size = 1000
    key_keyword = "tax:keyword"
    key_keyword_old = "tax:keywordOld"
    key_black_ip = "tax:blackIp"

    def parse(self,response):
        # print(response.text)
        host = "https://www.qichacha.com"
        xm = response.xpath("//div/section/a")
        if not xm and "index_verify" in response.text:
            request = response.request
            proxy = request.meta["proxy"]
            self.add_black(proxy)
            retryreq = self.retry(request)
            if retryreq:
                yield retryreq

        for i in xm:
            url = i.xpath("./@href").extract_first()
            if url:
                name = "".join(i.xpath("./div/div/span//text()").extract())
                meta = {"name":name}
                url_detail = host + url
                yield Request(url_detail,callback= self.parse_detail,priority = 50,meta = meta)

    def parse_detail(self,response):
        data = dict()
        com_addr = ""
        phone = ""
        bank = ""
        bankcard = ""
        creditNo = ""
        com_name = ""
        items = response.xpath("//form[@class='bs-example form-horizontal']/div")
        for i in items:
            title = "".join(i.xpath("./label/text()").extract())
            message = "".join(i.xpath("./div/p/text()").extract()).strip()
            if "税号" in title:
                creditNo = message
            elif "企业地址" in title:
                com_addr = message
            elif "电话号码" in title:
                phone = message
            elif "开户银行" in title:
                bank = message
            elif "银行账户" in title:
                bankcard = message
            elif "企业名称" in title:
                com_name = message
        if com_name:
            data["company"] = com_name
            data["creditNo"] = creditNo
            data["com_addr"] = com_addr
            data["phone"] = phone
            data["bank"] = bank
            data["bankcard"] = bankcard
            return data

    def spider_idle(self):
        server = self.server
        for i in range(self.redis_batch_size):
            keyword = server.spop(self.key_keyword)
            if keyword:
                keyword = keyword.decode()
                added = server.sadd(self.key_keyword_old, keyword)
                if added != 0:
                    url = "https://www.qichacha.com/tax_search?key=" + keyword
                    req = Request(url, callback=self.parse, dont_filter=True)
                    self.crawler.engine.crawl(req, spider=self)
            else:
                break
        raise DontCloseSpider



    def retry(self,request):
        retries = request.meta.get('retry_times', 0) + 1

        retry_times = self.settings.getint('RETRY_TIMES')

        if 'max_retry_times' in request.meta:
            retry_times = request.meta['max_retry_times']

        if retries <= retry_times:
            logger.debug("Retrying %(request)s (failed %(retries)d times)",
                         {'request': request, 'retries': retries},
                         extra={'spider': self.name})
            retryreq = request.copy()
            retryreq.meta['retry_times'] = retries
            del retryreq.meta["proxy"]
            retryreq.dont_filter = True
            retryreq.priority = request.priority
            return retryreq

    def add_black(self,proxy):
        proxy = proxy.strip("http://")
        ip = proxy.split(":")[0]
        server = self.server
        server.sadd(self.key_black_ip,ip)