# -*- coding: utf-8 -*-

import json


class TaxPipeline(object):

    def __init__(self,key):
        self.key = key

    @classmethod
    def from_crawler(cls,crawler):
        settings = crawler.settings
        key = settings.get("REDIS_DATA_KEY")
        if not key:
            key = "tax:data"
        return cls(key)

    def process_item(self, item, spider):
        server = spider.server
        data = json.dumps(item,ensure_ascii=False)
        server.sadd(self.key,data)
        return item
