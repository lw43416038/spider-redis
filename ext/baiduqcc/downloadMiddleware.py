import six
from scrapy import Request
from six.moves.urllib.parse import urljoin
from w3lib.url import safe_url_string
from scrapy.downloadermiddlewares.retry import RetryMiddleware
from scrapy.downloadermiddlewares.redirect import BaseRedirectMiddleware

key = "bdqcc:proxy"
basic_priority = 50
retry_callback_method = "parse"

def request_retry(request,spider):
    # retryreq = request.copy()
    meta = request.meta
    if "url" in meta:
        url = meta["url"]
        priority = basic_priority
        callback = getattr(spider, retry_callback_method)
    else:
        url = request.url
        priority = request.priority
        callback = request.callback
    retries = meta.get('retry_times', 0) + 1
    meta["retry_times"] = retries

    if "proxy" in meta:
        del meta["proxy"]
    retryreq = Request(url,callback=callback,meta= meta,priority= priority,dont_filter=True)

    return retryreq

class Retry(RetryMiddleware):

    def _retry(self, request, reason, spider):
        retryreq = request_retry(request,spider)
        return retryreq

class Proxy():
    def process_request(self,request,spider):
        if 'proxy' not in request.meta:
            proxy = spider.server.srandmember(key).decode()
            request.meta["proxy"] = "http://" + proxy

class Redirect(BaseRedirectMiddleware):

    def process_response(self,request, response, spider):

        allowed_status = (301, 302, 303, 307, 308)
        if 'Location' not in response.headers or response.status not in allowed_status:
            return response

        location = safe_url_string(response.headers['location'])
        redirected_url = urljoin(request.url, location)
        #have verifyCode
        if redirected_url.startswith("https://xin.baidu.com/fs/check"):
            return request_retry(request,spider)
        redirected = self._redirect_request_using_get(request, redirected_url)
        return self._redirect(redirected, request, spider, response.status)
