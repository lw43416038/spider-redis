

if __name__ == '__main__':
    import os
    import shutil
    import sys
    import setting
    from dis_utils import dis
    argv = sys.argv
    no = 0
    spider = None
    operate = None
    while no < len(argv):
        param = argv[no]
        if param == "-c":
            if len(argv) > no +1:
                spider = argv[no + 1]
                no += 1
            elif spider:
                pass
            else:
                raise ValueError("no spider")
        elif param == "make":
            operate = "make"
        elif param == "start":
            operate = "start"
        elif param == "close":
            operate = 'close'
        elif param == "update":
            operate = 'update'
        no += 1
    if not operate:
        raise ValueError("must give operate ")
    if not spider:
        raise ValueError("must give spider")
    # assert spider in setting.PROJECT_INFO
    if "make"  == operate:
        print("start add spider<%s> to spider_ds"%spider)
        project = setting.PROJECT_INFO.get(spider, spider)
        is_scrapy_redis = setting.PROJECT_CONFIG[spider]["scrapy_redis"]
        dis.spider_to_dis(project,is_scrapy_redis = is_scrapy_redis )
    elif "start" == operate:
        print("start dis_utils {spider}".format(spider = spider))
        from dis_utils.start import main
        dont_filter = setting.PROJECT_CONFIG[spider]["dont_filter"]
        flush = setting.PROJECT_CONFIG[spider]["flush"]
        main(spider,dont_filter=dont_filter,flush=flush)
    elif "update" == operate:
        print("=========update {spider}=======".format(spider=spider))
        project = setting.PROJECT_INFO.get(spider, spider)
        is_scrapy_redis = setting.PROJECT_CONFIG[spider]["scrapy_redis"]
        dis.update(project, is_scrapy_redis=is_scrapy_redis)