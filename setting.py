
#项目配置
PROJECT_INFO = {
    "weather":"weather",
    "bdqcc":"baiduqcc",
    "tax":"tax"
}

PROJECT_CONFIG = {
    "weather":{
        "dont_filter":True,
        "flush":True,
        "scrapy_redis":False
    },
    "bdqcc":{
            "dont_filter":False,
            "flush":False,
            "scrapy_redis":True
        },
    "tax":{
            "dont_filter":False,
            "flush":False,
            "scrapy_redis":True
            }
}
